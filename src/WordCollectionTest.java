import static org.junit.Assert.*;

import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class WordCollectionTest {

    public void testWordEntry() throws FileNotFoundException {
        Scanner input = new Scanner(new File("data/dict1.txt"));
        Set<String> dictionary = new TreeSet<String>();
        while (input.hasNextLine()) {
            dictionary.add(input.nextLine());
        }
        dictionary = Collections.unmodifiableSet(dictionary);
        WordCollection wc = new WordCollection(dictionary);
        StringBuilder sb = new StringBuilder();
        Scanner checkFile = new Scanner(new File("data/dict1output.txt"));
        while (checkFile.hasNextLine()) {
            sb.append(checkFile.nextLine());
            sb.append("\n");
        }
        assertTrue(sb.toString().equals(wc.toString()));
    }


    public void testContainsNormal() throws FileNotFoundException {
        Scanner input = new Scanner(new File("data/dict1.txt"));
        Set<String> dictionary = new TreeSet<String>();
        while (input.hasNextLine()) {
            dictionary.add(input.nextLine());
        }
        dictionary = Collections.unmodifiableSet(dictionary);
        WordCollection wc = new WordCollection(dictionary);
        assertTrue(wc.contains("here"));
        assertFalse(wc.contains("heres"));

    }
}
