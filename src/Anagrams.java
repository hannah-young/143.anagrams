import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by hannah on 5/25/14.
 */
public class Anagrams {
    private final Set<String> dictionary; //dictionary for this anagram

    /**
     * Create a new anagram solver with the specified dictionary. Dictionary is not modified by this
     * class.
     *
     * @param dictionary dictionary to use for this anagram solver
     */
    public Anagrams(Set<String> dictionary) {
        if (dictionary == null) {
            throw new IllegalArgumentException("Dictionary is null");
        }
        this.dictionary = dictionary;
    }

    /**
     * Finds all the words that can be created using any number of letters from the specified
     * phrase. Words are chosen only from this instance's dictionary.
     *
     * @param phrase phrase to find words of
     * @return all of the words that can be created from the phrase
     * @throws java.lang.IllegalArgumentException if phrase is null
     */
    public Set<String> getWords(String phrase) {
        if (phrase == null) {
            throw new IllegalArgumentException("Null phrase");
        }
        Set<String> matchingWords = new HashSet<String>(); //use HashSet for fast add times
        ImmutableLetterInventory word = new ImmutableLetterInventory(phrase);
        for (String inDict : dictionary) {
            if (word.contains(inDict)) {
                matchingWords.add(inDict);
            }
        }
        return new TreeSet<String>(matchingWords); //copy into TreeSet so it's sorted
    }

    /**
     * Prints anagrams that can be found using the specified phrase without exceeding the specified
     * word limit for each anagram. A limit of 0 will print every anagram possible.
     *
     * @param phrase phrase to find anagrams of
     * @param max    max number of words to include in each individual anagram solution
     * @throws java.lang.IllegalArgumentException if phrase is null
     * @throws java.lang.IllegalArgumentException if max is less than 0
     */
    public void print(String phrase, int max) {
        if (phrase == null) {
            throw new IllegalArgumentException("Null phrase");
        }
        if (max < 0) {
            throw new IllegalArgumentException("Max less than 0: " + max);
        }


    }

    private Set<String> findAnagrams(Set<String> anagrams,
        ImmutableLetterInventory phrase, ArrayList<String> dict, ArrayList<String> currentAnagram, int max) {
        if (phrase.isEmpty()) {
            anagrams.add(anagramStringFromList(currentAnagram));
            return anagrams;
        }
        if (max == 0 || currentAnagram.size() < max) {
            for (int i = 0; i < dict
                .size(); i++) { //for every word in the dictionary that can be found in this phrase...

                String dictWord = dict.get(i);
                if (phrase.contains(dictWord)) { //..if that word is in the phrase:
                    ArrayList<String> thisWordList =
                        new ArrayList<String>(currentAnagram); //make copy of word list
                    thisWordList.add(dictWord); //add the word to the list
                    findAnagrams(anagrams, phrase.subtract(dictWord), dict, thisWordList,
                        max); //and see if you can find any other words in the phrase
                }
            }
        }
        return anagrams;
    }

    /**
     * Prints all anagrams that can be found using the specified phrase. Anagrams will only be
     * created using this instance's dictionary.
     *
     * @param phrase phrase to find anagrams of
     * @throws java.lang.IllegalArgumentException if phrase is null
     */
    public void print(String phrase) {
        print(phrase, 0); //simply send to print with a limit of 0
    }

    public Set<String> getAnagrams(String phrase, int max) {
        ArrayList<String> possibleWords = new ArrayList<String>(getWords(phrase));
        Set<String> anagrams = new TreeSet<String>();
        findAnagrams(anagrams, new ImmutableLetterInventory(phrase), possibleWords, new ArrayList<String>(), max);
        return anagrams;
    }

    private static String anagramStringFromList(ArrayList<String> anagramWords) {
        StringBuilder ana = new StringBuilder();
        for (String s : anagramWords) {
            ana.append(s);
            ana.append(", ");
        }
        return ana.substring(0, ana.length() - 2);
    }
}
