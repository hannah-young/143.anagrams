import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.regex.Pattern;

import static org.junit.Assert.*;

public class AnagramsTest {
    private Anagrams a;

    @Before
    public void setUp() throws Exception {
        Set<String> dictionary = getSetFromFile("data/dict3.txt");
        a = new Anagrams(dictionary);
    }

    @Test
    public void testFindWords() {
        Set<String> test = a.getWords("barbara bush");
        Set<String> expected = getSetFromFile("data/test/bbushwords.txt");
        assertEquals("Sets should be equal size", test.size(), expected.size());
        assertTrue("One set should contain the other", test.containsAll(expected));
        assertTrue("And the other should contain the first", expected.containsAll(test)); //probably not necessary
    }

    @Test
    public void testAnagramsNoMax() {
        Set<String> test = a.getAnagrams("barbara bush", 0);
        Set<String> expected = getSetFromFile("data/test/bbushanagrams.txt");
        assertEquals("Sets should be equal size", test.size(), expected.size());
        assertTrue("One set should contain the other", test.containsAll(expected));
        assertTrue("And the other should contain the first", expected.containsAll(test)); //probably not necessary
    }

    @Test
    public void testAnagramsWithMax() {
        Set<String> test = a.getAnagrams("barbara m bush", 3);
        Set<String> expected = getSetFromFile("data/test/bmbushanagrams.txt");
        Iterator<String> testIt = test.iterator();
        Iterator<String> expectedIt = expected.iterator();
        while (testIt.hasNext() && expectedIt.hasNext()) {
            System.out.printf("Expected: %s\n", expectedIt.next());
            System.out.printf("Actual:   %s\n\n", testIt.next());
        }
        assertEquals("Sets should be equal size", test.size(), expected.size());
        assertTrue("One set should contain the other", test.containsAll(expected));
        assertTrue("And the other should contain the first", expected.containsAll(test)); //probably not necessary
    }

    private Set<String> getSetFromFile(String fileName) {
        Scanner input = null;
        try {
            input = new Scanner(new File(fileName));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        input.useDelimiter(Pattern.compile("\n"));
        Set<String> wordSet = new TreeSet<String>();
        System.out.println(input.delimiter());
        while (input.hasNextLine()) {
            wordSet.add(input.nextLine());
        }
        return wordSet;
    }

}
