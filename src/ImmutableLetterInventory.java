/**
 * Created by hannah on 5/25/14.
 */
public class ImmutableLetterInventory {
    private final LetterInventory letter;

    public ImmutableLetterInventory(String s) {
        letter = new LetterInventory(s);
    }

    public boolean contains(LetterInventory other) {
        return letter.contains(other);
    }

    public boolean contains(String s) {
        return letter.contains(s);
    }

    public boolean isEmpty() {
        return letter.isEmpty();
    }

    public int size() {
        return letter.size();
    }

    public ImmutableLetterInventory subtract(LetterInventory other) {
        String stringCopy = letter.toString();
        LetterInventory inventoryCopy = new LetterInventory(stringCopy);
        inventoryCopy.subtract(other);
        return new ImmutableLetterInventory(inventoryCopy.toString());
    }

    public ImmutableLetterInventory subtract(String s) {
        return subtract(new LetterInventory(s));
    }
}
