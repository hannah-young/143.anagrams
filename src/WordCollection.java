import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by hannah on 5/22/14.
 */
public class WordCollection {
    private List<String>[][] words;

    public WordCollection(Set<String> dict) {
        words = new List[26][27];
        for (int i = 0; i < words.length; i++) {
            for (int j = 0; j < words[i].length; j++) {
                words[i][j] = new ArrayList<String>();
            }
        }
        for (String s: dict) {
            //TODO: s length 0/1
            char a = Character.toLowerCase(s.charAt(0)); //TODO: find better non-alpha test
            char b = Character.toLowerCase(s.charAt(1));
            if (!Character.isAlphabetic(a) || !Character.isAlphabetic(b)) { //chars must be alpha
                throw new IllegalArgumentException("Character in word " + s + " is not alphabetic.");
            }
            int aInt = a - 'a';
            int bInt = b - 'a';
            words[aInt][bInt].add(s);
        }
    }

    @Override public String toString() {
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < words.length; i++) {
            for (int j = 0; j < words.length; j++) {
                char iChar = (char) ('a' + i);
                char jChar = (char) ('a' + j);
                while (!words[i][j].isEmpty()) {
                    s.append(iChar + "/" + jChar + ": " + words[i][j].remove(0) + "\n");
                }
            }
        }
        return s.toString();
    }

    public boolean contains(String s) {
        //TODO: implement String length = 0/1
        //TODO: check for non-alpha
        int a = Character.toLowerCase(s.charAt(0)) - 'a';
        int b = Character.toLowerCase(s.charAt(1)) - 'a';
        return words[a][b].contains(s);
    }

}
